/*
 * basic_Shell.c
 *
 *  Created on: 2014-07-28
 *      Author: Adrien Lapointe
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>

const unsigned int cmd_max_len = 1024;
const unsigned int max_num_arg = 64;


void welcome_prompt();
void parse(char *input, char **argv);
void type_prompt(char *input);
void interpret_command(char **argv);



int main() {

	char input[cmd_max_len];             // the input
	char *argv[max_num_arg];              // the arguments

	int pid, status;

	welcome_prompt();

	while (1) {

		type_prompt(input);
		pid = waitpid(-1, &status, WNOHANG); // This is necessary for the fork.

		parse(input, argv);

		interpret_command(argv);

	}

	return 0;
}

/*
 * This functions prints the prompt message and accepts input from the user.
 */
void type_prompt(char *input) {

	printf("EEE435$: ");

	if (input != NULL) {
		int c = EOF;
		int i = 0;

		// take in input until user hits enter or end of file is encountered.
		while ((c = getchar()) != '\n' && c != EOF) {
			input[i++] = (char) c;
		}
		for (i; i<64; i++){
			input[i] = '\0';
		}
	}
}

/*
 * This function parses the user inputs.
 */
void parse(char *input, char **argv) {

	// This is where the code for parsing the user's input is.
	// C'est l'endroit ou vous placez le code pour separer
	// les entree de l'utilisateur.
	int count = 0;
	char * token;

	token = strtok(input, " "); //holds tokens in the string

	//store the rest of the tokens in the arguments array
	while (token != NULL) {
		argv[count] = token;
		token = strtok( NULL, " ");
		count++;
	}
}

/*
 * This function interprets the parsed command that is entered by the user and
 * calls the appropriate built-in function or calls the appropriate program.
 */
void interpret_command(char **argv) {
	// This is where you will write code to call the appropriate function or program.
	// L'endroit ou vous appelerez les fonctions ou programmes appropries.

	pid_t fork_return;


	if (!strcmp(argv[0], "pwd")) {
		char* dir = get_current_dir_name();
		printf("%s\n", dir);
		free(dir);
	} else if (!strcmp(argv[0], "cd")) {
		chdir(argv[1]);
	} else if (!strcmp(argv[0], "ls")) {
        DIR *dir;
        struct dirent *dirs;
        dir = opendir(".");
        if (dir) {
            while ((dirs = readdir(dir)) != NULL) {
            	if ((argv[1] && !strcmp(argv[1], "-a")) || ((*dirs).d_name[0] != '.')){
            		printf("%s\n", (*dirs).d_name);
            	}
            }
            closedir(dir);
        }
	} else if (!strcmp(argv[0], "exit")) {
		printf("Bye\n");
		exit(0);
	} else {
		char * command = (char *)malloc(100*sizeof(char));
		sprintf(command, "/usr/bin/%s", argv[0]);
		if (strcmp(command, "/usr/bin/") && access(command, F_OK ) != -1)
		{
			fork_return  = fork( );

			   if (fork_return == 0)
			   {
			      execv(command, argv);
			      exit(0);
			   } else if(fork_return > 0) {
			      wait();
			   }
			   else if(fork_return == -1) {
			      printf("ERROR:\n");
			      switch (errno)
			      {
			      case EAGAIN:
				    printf("Cannot fork process: System Process Limit Reached\n");
				    break;
				  case ENOMEM:
				    printf("Cannot fork process: Out of memory\n");
				    break;
			      }
			   }
		} else {
			printf("Unrecognized Command.\n");
		}
	}
}

/*
 * This function prints the welcome message.
 */
void welcome_prompt() {
	int num_Padding = 41;
	int i;

// Prints the first line of padding.
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n#\tWelcome to the EEE435 shell\t#\n");
	for (i = 0; i < num_Padding; i++)
		printf("#");
	printf("\n\n");
}
